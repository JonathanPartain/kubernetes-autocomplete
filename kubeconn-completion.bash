#!/usr/bin/env bash
kubedata=$(kubectl get pods -A)
getwords() {
	local cur
	cur="${COMP_WORDS[COMP_CWORD]}"
	case $COMP_CWORD in 
		1)
		COMPREPLY=( $(compgen -W "`echo "$kubedata" | awk '{if (NR!=1) print $2}' | uniq | cat`" -- ${cur} ) )
		;;
		2)
		COMPREPLY=( $(compgen -W "`echo "$kubedata" | awk '{if (NR!=1) print $1}' | uniq | cat`" -- ${cur} ) )
		;;
		3)
		COMPREPLY=( $(compgen -W "bash sh" -- ${cur} ) )
		;;
	esac
}
complete -F getwords kubeconn
