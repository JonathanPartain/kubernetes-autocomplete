# Kubernetes autocomplete
## Linux instructions
Copy `kubeconn` and `kubeconn-completion.bash` to `/usr/local/bin/` (this will requre root access).

Make sure that `kubeconn` is executabel by running `sudo chmod +x /usr/local/bin/kubeconn`

add the following two lines to your `~/.bashrc` file:
```
source /usr/local/bin/kubeconn-completion.bash
source /usr/local/bin/kubeconn
```

## Usage
`kubeconn <tab><tab>` choose what pod to connect to

`kubeconn <pod> <tab><tab>` select namespace

`kubeconn <pod> <namespace> <enter>` done!
